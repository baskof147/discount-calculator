<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\exceptions;

use Exception;

/**
 * Class InvalidTypeException
 * @package baskof147\discount_calculator\exceptions
 */
class InvalidTypeException extends Exception
{
}

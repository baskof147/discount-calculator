<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\product;

use baskof147\discount_calculator\exceptions\InvalidTypeException;
use baskof147\discount_calculator\product\interfaces\ProductCollectionInterface;
use baskof147\discount_calculator\product\interfaces\ProductInterface;

/**
 * Class ProductCollection
 * @package baskof147\discount_calculator\product
 */
class ProductCollection implements ProductCollectionInterface
{
    /**
     * @var ProductInterface[]
     */
    private array $_products;

    /**
     * ProductCollection constructor.
     * @param ProductInterface[] $products
     * @throws InvalidTypeException
     * @psalm-suppress DocblockTypeContradiction
     */
    public function __construct(array $products = [])
    {
        foreach ($products as $product) {
            if (!$product instanceof ProductInterface) {
                throw new InvalidTypeException('Every $products item must be instance of ProductInterface');
            }
        }
        $this->_products = $products;
    }

    /**
     * @inheritDoc
     */
    public function getProducts(): array
    {
        return $this->_products;
    }

    /**
     * @param ProductInterface $product
     */
    public function setProduct(ProductInterface $product): void
    {
        $this->_products[] = $product;
    }
}

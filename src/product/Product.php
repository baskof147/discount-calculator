<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\product;

use baskof147\discount_calculator\product\interfaces\ProductInterface;

/**
 * Class Product
 * @package baskof147\discount_calculator\product
 */
class Product implements ProductInterface
{
    private const DEFAULT_COUNT = 1;

    private string $_primaryKey;
    private float $_price;
    private int $_count;

    /**
     * Product constructor.
     * @param string $primaryKey
     * @param float $price
     * @param int $count
     */
    public function __construct(string $primaryKey, float $price, int $count = self::DEFAULT_COUNT)
    {
        $this->_primaryKey = $primaryKey;
        $this->_price = $price;
        $this->_count = $count;
    }

    /**
     * @inheritDoc
     */
    public function getPrimaryKey(): string
    {
        return $this->_primaryKey;
    }

    /**
     * @inheritDoc
     */
    public function getPrice(): float
    {
        return $this->_price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->_price = $price;
    }

    /**
     * @inheritDoc
     */
    public function getCount(): int
    {
        return $this->_count;
    }
}

<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\product\interfaces;

/**
 * Interface ProductInterface
 * @package baskof147\discount_calculator\product\interfaces
 */
interface ProductInterface
{
    /**
     * @return string
     */
    public function getPrimaryKey(): string;

    /**
     * @return float
     */
    public function getPrice(): float;

    /**
     * @param float $price
     */
    public function setPrice(float $price): void;

    /**
     * @return int
     */
    public function getCount(): int;
}

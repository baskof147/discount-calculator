<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\product\interfaces;

/**
 * Interface ProductCollectionInterface
 * @package baskof147\discount_calculator\product\interfaces
 */
interface ProductCollectionInterface
{
    /**
     * @param ProductInterface $product
     */
    public function setProduct(ProductInterface $product): void;

    /**
     * @return ProductInterface[]
     */
    public function getProducts(): array;
}

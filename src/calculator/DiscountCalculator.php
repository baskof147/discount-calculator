<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\calculator;

use baskof147\discount_calculator\calculator\interfaces\DiscountCalculatorInterface;
use baskof147\discount_calculator\discount\interfaces\DiscountCollectionInterface;
use baskof147\discount_calculator\product\interfaces\ProductCollectionInterface;
use baskof147\discount_calculator\product\interfaces\ProductInterface;

/**
 * Class DiscountCalculator
 * @package baskof147\discount_calculator\calculator
 */
class DiscountCalculator implements DiscountCalculatorInterface
{
    private ProductCollectionInterface $_productCollection;
    private DiscountCollectionInterface $_discountCollection;

    /**
     * Store constructor.
     * @param ProductCollectionInterface $productCollection
     * @param DiscountCollectionInterface $discountCollection
     */
    public function __construct(ProductCollectionInterface $productCollection, DiscountCollectionInterface $discountCollection)
    {
        $this->_productCollection = $productCollection;
        $this->_discountCollection = $discountCollection;
    }

    /**
     * @inheritDoc
     */
    public function getTotalPriceWithoutDiscount(): float
    {
        return $this->getPrice($this->_productCollection);
    }

    /**
     * @inheritDoc
     */
    public function getTotalPriceWithDiscount(): float
    {
        $products = $this->_productCollection;
        $ignoreProducts = [];

        foreach ($this->_discountCollection->getDiscounts() as $discount) {
            $discount->setIgnoreProductPrimaryKeys($ignoreProducts);
            $product = $discount->calculateProductDiscounts($products);
            $ignoreProducts = $discount->getIgnoreProductPrimaryKeys();
        }

        return $this->getPrice($products);
    }

    /**
     * @param ProductCollectionInterface $productCollection
     * @return float
     */
    private function getPrice(ProductCollectionInterface $productCollection): float
    {
        return array_sum(array_map(function (ProductInterface $product) {
            return $product->getCount() * $product->getPrice();
        }, $productCollection->getProducts()));
    }
}

<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\calculator\interfaces;

/**
 * Interface DiscountCalculatorInterface
 * @package baskof147\discount_calculator\calculator\interfaces
 */
interface DiscountCalculatorInterface
{
    /**
     * Return final total price for products without discounts
     * @return float
     */
    public function getTotalPriceWithoutDiscount(): float;

    /**
     * Return final total price for products include discounts
     * @return float
     */
    public function getTotalPriceWithDiscount(): float;
}

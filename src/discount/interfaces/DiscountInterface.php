<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\discount\interfaces;

use baskof147\discount_calculator\product\interfaces\ProductCollectionInterface;

/**
 * Interface DiscountInterface
 * @package baskof147\discount_calculator\discount
 */
interface DiscountInterface
{
    /**
     * @param ProductCollectionInterface $productCollection
     * @return ProductCollectionInterface
     */
    public function calculateProductDiscounts(ProductCollectionInterface $productCollection): ProductCollectionInterface;

    /**
     * @param array $primaryKeys
     */
    public function setIgnoreProductPrimaryKeys(array $primaryKeys = []): void;

    /**
     * @return array
     */
    public function getIgnoreProductPrimaryKeys(): array;
}

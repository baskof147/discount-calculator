<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\discount\interfaces;

/**
 * Interface DiscountCollectionInterface
 * @package baskof147\discount_calculator\discount\interfaces
 */
interface DiscountCollectionInterface
{
    /**
     * @param DiscountInterface $discount
     */
    public function setDiscount(DiscountInterface $discount): void;

    /**
     * @return DiscountInterface[]
     */
    public function getDiscounts(): array;
}

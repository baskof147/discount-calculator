<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\discount;

use baskof147\discount_calculator\discount\interfaces\DiscountInterface;
use baskof147\discount_calculator\product\interfaces\ProductCollectionInterface;
use baskof147\discount_calculator\product\interfaces\ProductInterface;

/**
 * Class AbstractDiscount
 * @package baskof147\discount_calculator\discount
 */
abstract class AbstractDiscount implements DiscountInterface
{
    protected float $discount = 0;
    private array $_ignorePrimaryKey = [];

    /**
     * @param ProductInterface $product
     * @return float
     */
    protected function getPriceWithDiscount(ProductInterface $product): float
    {
        return $product->getPrice() * ((100 - $this->discount) / 100);
    }

    /**
     * @param ProductCollectionInterface $productCollection
     * @return array
     */
    protected function getProductsPrimaryKeys(ProductCollectionInterface $productCollection): array
    {
        return array_map(function (ProductInterface $product) {
            return $product->getPrimaryKey();
        }, $productCollection->getProducts());
    }

    /**
     * @param array $primaryKeys
     */
    public function setIgnoreProductPrimaryKeys(array $primaryKeys = []): void
    {
        $this->_ignorePrimaryKey = array_unique(array_merge($this->_ignorePrimaryKey, $primaryKeys));
    }

    /**
     * @return array
     */
    public function getIgnoreProductPrimaryKeys(): array
    {
        return $this->_ignorePrimaryKey;
    }
}

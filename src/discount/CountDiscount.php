<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\discount;

use baskof147\discount_calculator\product\interfaces\ProductCollectionInterface;

/**
 * Class CountDiscount
 * @package baskof147\discount_calculator\discount
 */
class CountDiscount extends AbstractDiscount
{
    /**
     * @var int
     */
    private int $_count;

    /**
     * CountDiscount constructor.
     * @param int $count
     * @param float $discount
     */
    public function __construct(int $count, float $discount)
    {
        $this->_count = $count;
        $this->discount = $discount;
    }

    /**
     * @inheritDoc
     */
    public function calculateProductDiscounts(ProductCollectionInterface $productCollection): ProductCollectionInterface
    {
        $primaryKeys = $this->getProductsPrimaryKeys($productCollection);
        $countablePrimaryKeys = array_filter($primaryKeys, function (string $primaryKey) {
            return !in_array($primaryKey, $this->getIgnoreProductPrimaryKeys());
        });
        if (count($countablePrimaryKeys) !== $this->_count) {
            return $productCollection;
        }

        foreach ($productCollection->getProducts() as $product) {
            if (!in_array($product->getPrimaryKey(), $countablePrimaryKeys)) {
                continue;
            }

            $product->setPrice($this->getPriceWithDiscount($product));
        }

        $this->setIgnoreProductPrimaryKeys($countablePrimaryKeys);
        return $productCollection;
    }
}

<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\discount;

use baskof147\discount_calculator\product\interfaces\ProductCollectionInterface;

/**
 * Class PairDiscount
 * @package baskof147\discount_calculator\discount
 */
class PairDiscount extends AbstractDiscount
{
    private array $_primaryKeysConfig;

    /**
     * PairDiscount constructor.
     * @param array $primaryKeysConfig
     * @param float $discount
     */
    public function __construct(array $primaryKeysConfig, float $discount)
    {
        $this->_primaryKeysConfig = $primaryKeysConfig;
        $this->discount = $discount;
    }

    /**
     * @inheritDoc
     */
    public function calculateProductDiscounts(ProductCollectionInterface $productCollection): ProductCollectionInterface
    {
        $primaryKeys = $this->getProductsPrimaryKeys($productCollection);
        if (!$this->productsHaveFullEntry($primaryKeys)) {
            return $productCollection;
        }

        /** @var array|string $config */
        foreach ($this->_primaryKeysConfig as $config) {
            $arrayIntersect = array_intersect($primaryKeys, (array) $config);
            /** @var string $primaryKeyToDiscount */
            $primaryKeyToDiscount = array_shift($arrayIntersect);

            foreach ($productCollection->getProducts() as $product) {
                if (in_array($product->getPrimaryKey(), $this->getIgnoreProductPrimaryKeys())) {
                    continue;
                }

                if ($product->getPrimaryKey() === $primaryKeyToDiscount) {
                    $product->setPrice($this->getPriceWithDiscount($product));
                    $this->setIgnoreProductPrimaryKeys([$product->getPrimaryKey()]);
                }
            }
        }

        return $productCollection;
    }

    /**
     * @param array $primaryKeys
     * @return bool
     */
    private function productsHaveFullEntry(array $primaryKeys): bool
    {
        /** @var array|string $config */
        foreach ($this->_primaryKeysConfig as $config) {
            $arrayIntersect = array_intersect($primaryKeys, (array) $config);
            if (!$arrayIntersect) {
                return false;
            }
        }

        return true;
    }
}

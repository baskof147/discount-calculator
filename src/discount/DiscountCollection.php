<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\discount;

use baskof147\discount_calculator\discount\interfaces\DiscountCollectionInterface;
use baskof147\discount_calculator\discount\interfaces\DiscountInterface;
use baskof147\discount_calculator\exceptions\InvalidTypeException;

/**
 * Class DiscountCollection
 * @package baskof147\discount_calculator\discount
 */
class DiscountCollection implements DiscountCollectionInterface
{
    /**
     * @var DiscountInterface[]
     */
    private array $_discounts;

    /**
     * DiscountCollection constructor.
     * @param DiscountInterface[] $discounts
     * @throws InvalidTypeException
     * @psalm-suppress DocblockTypeContradiction
     */
    public function __construct(array $discounts = [])
    {
        foreach ($discounts as $discount) {
            if (!$discount instanceof DiscountInterface) {
                throw new InvalidTypeException('Every $discounts item must be instance of DiscountInterface');
            }
        }
        $this->_discounts = $discounts;
    }

    /**
     * @inheritDoc
     */
    public function getDiscounts(): array
    {
        return $this->_discounts;
    }

    /**
     * @param DiscountInterface $discount
     */
    public function setDiscount(DiscountInterface $discount): void
    {
        $this->_discounts[] = $discount;
    }
}

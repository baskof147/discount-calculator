<?php

declare(strict_types=1);

namespace baskof147\discount_calculator\discount;

use baskof147\discount_calculator\exceptions\InvalidTypeException;
use baskof147\discount_calculator\product\interfaces\ProductCollectionInterface;
use baskof147\discount_calculator\product\ProductCollection;

/**
 * Class StaticDiscount
 * @package baskof147\discount_calculator\discount
 */
class StaticDiscount extends AbstractDiscount
{
    /**
     * StaticDiscount constructor.
     * @param float $discount
     */
    public function __construct(float $discount)
    {
        $this->discount = $discount;
    }

    /**
     * @inheritDoc
     * @throws InvalidTypeException
     */
    public function calculateProductDiscounts(ProductCollectionInterface $productCollector): ProductCollectionInterface
    {
        $products = [];
        foreach ($productCollector->getProducts() as $product) {
            $product->setPrice($this->getPriceWithDiscount($product));
            $products[] = $product;
        }
        return new ProductCollection($products);
    }
}

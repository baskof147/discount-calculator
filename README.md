Usage
-

Single product with single discount
```php
use baskof147\discount_calculator\calculator\DiscountCalculator;
use baskof147\discount_calculator\product\Product;
use baskof147\discount_calculator\product\ProductCollection;
use baskof147\discount_calculator\discount\StaticDiscount;
use baskof147\discount_calculator\discount\DiscountCollection;

$productCollection = new ProductCollection();
$productCollection->setProduct(new Product('A', 550));

$discountCollection = new DiscountCollection();
$discountCollection->setDiscount(new StaticDiscount(10));

$store = new DiscountCalculator($productCollection, $discountCollection);
return $store->getTotalPriceWithDiscount();
```

Multiple products with multiple discounts
```php
use baskof147\discount_calculator\calculator\DiscountCalculator;
use baskof147\discount_calculator\product\Product;
use baskof147\discount_calculator\product\ProductCollection;
use baskof147\discount_calculator\discount\StaticDiscount;
use baskof147\discount_calculator\discount\DiscountCollection;

$products = [
    new Product('A', 550),
    new Product('B', 330),
    new Product('C', 1100, 3)
];
$productCollection = new ProductCollection($products);

$discounts = [
    new StaticDiscount(10)
];
$discountCollection = new DiscountCollection($discounts);

$store = new DiscountCalculator($productCollection, $discountCollection);
return $store->getTotalPriceWithDiscount();
```

Psalm
-
`vendor/bin/psalm`

Tests
-
`vendor/bin/codecept run`
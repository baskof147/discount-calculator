<?php

declare(strict_types=1);

namespace unit;

use baskof147\discount_calculator\calculator\DiscountCalculator;
use baskof147\discount_calculator\discount\CountDiscount;
use baskof147\discount_calculator\discount\DiscountCollection;
use baskof147\discount_calculator\discount\PairDiscount;
use baskof147\discount_calculator\exceptions\InvalidTypeException;
use baskof147\discount_calculator\product\Product;
use baskof147\discount_calculator\product\ProductCollection;
use Codeception\Test\Unit;

/**
 * Class DiscountCalculatorAllTest
 * @package unit
 */
class DiscountCalculatorAllTest extends Unit
{
    /**
     * 1. Товары A и B получают 10% скидку: 495 + 351 + 390 + 45 + 850 + 3500 + 12321 + 963 + 478 + 500 + 1000 + 652 + 100
     * 2. Товары D и E получают 6% скидку: 495 + 351 + 390 + 42,3 + 799 + 3500 + 12321 + 963 + 478 + 500 + 1000 + 652 + 100
     * 3. Товар E уже получал скидку, товары F, G получают скидку 6%: 495 + 351 + 390 + 42,3 + 799 + 3290 + 11581,74 + 963 + 478 + 500 + 1000 + 652 + 100
     * 3. Товар A уже получал скидку, товар K получает скидку 5%: 495 + 351 + 390 + 42,3 + 799 + 3290 + 11581,74 + 963 + 478 + 500 + 950 + 652 + 100
     * 4. Товары, которые не получали скидку: H, I, L, M. Игнор правила кол-ва 3
     * 4. Товары, которые не получали скидку: H, I, L, M: 495 + 351 + 390 + 42,3 + 799 + 3290 + 11581,74 + 866,7 + 430,2 + 500 + 950 + 586,8 + 90
     * 4. Товары, которые не получали скидку: H, I, L, M. Игнор правила кол-ва 5
     * @throws InvalidTypeException
     */
    public function testAllDiscounts(): void
    {
        $productCollection = new ProductCollection([
            new Product('A', 550),
            new Product('B', 390),
            new Product('C', 390),
            new Product('D', 45),
            new Product('E', 850),
            new Product('F', 3500),
            new Product('G', 12321),
            new Product('H', 963),
            new Product('I', 478),
            new Product('G', 500),
            new Product('K', 1000),
            new Product('L', 652),
            new Product('M', 100),
        ]);

        $discountCollection = new DiscountCollection([
            new PairDiscount(['A', 'B'], 10),
            new PairDiscount(['D', 'E'], 6),
            new PairDiscount(['E', 'F', 'G'], 6),
            new PairDiscount(['A', ['K', 'L', 'M']], 5),
        ]);

        $discount = new CountDiscount(3, 5);
        $discount->setIgnoreProductPrimaryKeys(['A', 'C']);
        $discountCollection->setDiscount($discount);

        $discount = new CountDiscount(4, 10);
        $discount->setIgnoreProductPrimaryKeys(['A', 'C']);
        $discountCollection->setDiscount($discount);

        $discount = new CountDiscount(5, 20);
        $discount->setIgnoreProductPrimaryKeys(['A', 'C']);
        $discountCollection->setDiscount($discount);

        $store = new DiscountCalculator($productCollection, $discountCollection);

        //Цена без скидки
        $this->assertEquals(21739, $store->getTotalPriceWithoutDiscount());

        //Цена со скидкой
        $this->assertEquals(20372.74, $store->getTotalPriceWithDiscount());
    }
}

<?php

declare(strict_types=1);

namespace unit;

use baskof147\discount_calculator\exceptions\InvalidTypeException;
use baskof147\discount_calculator\product\ProductCollection;
use Codeception\Test\Unit;

/**
 * Class ProductCollectionTest
 * @package unit
 */
class ProductCollectionTest extends Unit
{
    public function testInvalidTypeException(): void
    {
        $this->expectException(InvalidTypeException::class);
        new ProductCollection([1, 2, 3]);
    }
}

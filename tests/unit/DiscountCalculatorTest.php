<?php

declare(strict_types=1);

namespace tests\unit;

use baskof147\discount_calculator\calculator\DiscountCalculator;
use baskof147\discount_calculator\discount\CountDiscount;
use baskof147\discount_calculator\discount\DiscountCollection;
use baskof147\discount_calculator\discount\PairDiscount;
use baskof147\discount_calculator\discount\StaticDiscount;
use baskof147\discount_calculator\exceptions\InvalidTypeException;
use baskof147\discount_calculator\product\Product;
use baskof147\discount_calculator\product\ProductCollection;
use Codeception\Specify;
use Codeception\Test\Unit;

/**
 * Class DiscountCalculatorTest
 * @package tests\unit
 */
class DiscountCalculatorTest extends Unit
{
    use Specify;

    /**
     * Проверяем, что товар суммой 550, имя скидку 10% вернет результат 550 - 55 = 495
     */
    public function testSingleProductStaticDiscount(): void
    {
        $productCollection = new ProductCollection();
        $productCollection->setProduct(new Product('A', 550));

        $discountCollection = new DiscountCollection();
        $discountCollection->setDiscount(new StaticDiscount(10));

        $store = new DiscountCalculator($productCollection, $discountCollection);
        $this->assertEquals(495, $store->getTotalPriceWithDiscount());
    }

    /**
     * Проверяем, что товар суммой 550 и 390, имя скидку 10% вернет результат (550 - 55) + (390 - 39) = 846
     */
    public function testMultipleProductsStaticDiscount(): void
    {
        $productCollection = new ProductCollection([
            new Product('A', 550),
            new Product('B', 390)
        ]);

        $discountCollection = new DiscountCollection();
        $discountCollection->setDiscount(new StaticDiscount(10));

        $store = new DiscountCalculator($productCollection, $discountCollection);
        $this->assertEquals(846, $store->getTotalPriceWithDiscount());
    }

    /**
     * Проверяем, что товар суммой 550 и 390, имя скидку 10% и 5% вернет результат (550 - 55 - 24,75) + (390 - 39 - 17,55) = 803.7
     */
    public function testMultipleProductsMultipleStaticDiscount(): void
    {
        $productCollection = new ProductCollection([
            new Product('A', 550),
            new Product('B', 390)
        ]);

        $discountCollection = new DiscountCollection([
            new StaticDiscount(10),
            new StaticDiscount(5)
        ]);

        $store = new DiscountCalculator($productCollection, $discountCollection);
        $this->assertEquals(803.7, $store->getTotalPriceWithDiscount());
    }

    /**
     * Проверяем, что товары A и D имеют скидку в 10, остальные без изменений
     * (550 - 55) + 390 + 480 + (435 - 43,5) = 1756.5
     * @throws InvalidTypeException
     */
    public function testSinglePairDiscount(): void
    {
        $productCollection = new ProductCollection([
            new Product('A', 550),
            new Product('B', 390),
            new Product('C', 480),
            new Product('D', 435),
        ]);

        $discountCollection = new DiscountCollection([
            new PairDiscount(['A', 'D'], 10),
        ]);

        $store = new DiscountCalculator($productCollection, $discountCollection);
        $this->assertEquals(1756.5, $store->getTotalPriceWithDiscount());
    }

    /**
     * Проверяем, что товары A и D имеют скидку в 10, остальные без изменений
     * (550 - 55) + 390 + 480 + (435 - 43,5) = 1756.5
     * @throws InvalidTypeException
     */
    public function testSinglePairWithEntry(): void
    {
        $productCollection = new ProductCollection([
            new Product('A', 550),
            new Product('B', 390),
            new Product('C', 480),
            new Product('D', 435),
        ]);

        $discountCollection = new DiscountCollection([
            new PairDiscount(['A', ['M', 'D']], 10),
        ]);

        $store = new DiscountCalculator($productCollection, $discountCollection);
        $this->assertEquals(1756.5, $store->getTotalPriceWithDiscount());
    }

    /**
     * Проверяем, что товары B и D имеют скидку в 10, остальные без изменений
     * 550 + (390 - 39) + 480 + (435 - 43,5) = 1772.5
     * @throws InvalidTypeException
     */
    public function testSinglePairWithMultipleEntry(): void
    {
        $productCollection = new ProductCollection([
            new Product('A', 550),
            new Product('B', 390),
            new Product('C', 480),
            new Product('D', 435),
        ]);

        $discountCollection = new DiscountCollection([
            new PairDiscount([['B', 'E', 'H'], ['M', 'D']], 10),
        ]);

        $store = new DiscountCalculator($productCollection, $discountCollection);
        $this->assertEquals(1772.5, $store->getTotalPriceWithDiscount());
    }

    /**
     * Проверяем, что совпадений только по одной позиции, возвращ дефолтных цен
     * 550 + 390 + 480 + 435 = 1855
     * @throws InvalidTypeException
     */
    public function testSinglePairWithNotFullEntry(): void
    {
        $productCollection = new ProductCollection([
            new Product('A', 550),
            new Product('B', 390),
            new Product('C', 480),
            new Product('D', 435),
        ]);

        $discountCollection = new DiscountCollection([
            new PairDiscount(['A', 'Z'], 10),
        ]);

        $store = new DiscountCalculator($productCollection, $discountCollection);
        $this->assertEquals(1855, $store->getTotalPriceWithDiscount());
    }

    /**
     * Проверяем, что B в игноре и делаем скидку только для позиции A
     * @throws InvalidTypeException
     */
    public function testPrimaryKeysInIgnore(): void
    {
        $productCollection = new ProductCollection([
            new Product('A', 550),
            new Product('B', 390),
            new Product('C', 480),
            new Product('D', 435),
        ]);

        $discount = new PairDiscount(['A', 'B'], 10);
        $discount->setIgnoreProductPrimaryKeys(['B']);
        $discountCollection = new DiscountCollection();
        $discountCollection->setDiscount($discount);

        $store = new DiscountCalculator($productCollection, $discountCollection);
        $this->assertEquals(1800, $store->getTotalPriceWithDiscount());
    }

    /**
     * Проверяем, все позиции в игноре, возвращает дефолтную цену
     * @throws InvalidTypeException
     */
    public function testAllPrimaryKeysInIgnore(): void
    {
        $productCollection = new ProductCollection([
            new Product('A', 550),
            new Product('B', 390),
            new Product('C', 480),
            new Product('D', 435),
        ]);

        $discount = new PairDiscount(['A', 'B'], 10);
        $discount->setIgnoreProductPrimaryKeys(['A', 'B']);
        $discountCollection = new DiscountCollection();
        $discountCollection->setDiscount($discount);

        $store = new DiscountCalculator($productCollection, $discountCollection);
        $this->assertEquals(1855, $store->getTotalPriceWithDiscount());
    }

    /**
     * Проверяем, что все товары получили скидку 10%, кроме игнора
     * (550 - 55) + (390 - 39) + (480 - 48) + (435 - 43,5) + 200 + (2500 - 250) + 1985 = 6104,5
     * @throws InvalidTypeException
     */
    public function testCountDiscount(): void
    {
        $productCollection = new ProductCollection([
            new Product('A', 550),
            new Product('B', 390),
            new Product('C', 480),
            new Product('D', 435),
            new Product('E', 200),
            new Product('F', 2500),
            new Product('G', 1985),
        ]);
        $discountCollection = new DiscountCollection();
        $discount = new CountDiscount(5, 10);
        $discount->setIgnoreProductPrimaryKeys(['E', 'G']);
        $discountCollection->setDiscount($discount);

        $store = new DiscountCalculator($productCollection, $discountCollection);
        $this->assertEquals(6104.5, $store->getTotalPriceWithDiscount());
    }

    /**
     * Проверяем, что кол-во товаров меньше, чем нужно, возврат дефолтных цен
     * 550 + 390 + 480 = 1420
     * @throws InvalidTypeException
     */
    public function testCountDiscountByLessCount(): void
    {
        $productCollection = new ProductCollection([
            new Product('A', 550),
            new Product('B', 390),
            new Product('C', 480),
        ]);
        $discountCollection = new DiscountCollection();
        $discountCollection->setDiscount(new CountDiscount(4, 10));

        $store = new DiscountCalculator($productCollection, $discountCollection);
        $this->assertEquals(1420, $store->getTotalPriceWithDiscount());
    }

    /**
     * Проверяем, что кол-во товаров достаточно, но одна позиция в игноре, возврат дефолт
     * 550 + 390 + 480 + 435 = 1855
     * @throws InvalidTypeException
     */
    public function testCountDiscountByLessCountCauseOfIgnoredKey(): void
    {
        $productCollection = new ProductCollection([
            new Product('A', 550),
            new Product('B', 390),
            new Product('C', 480),
            new Product('D', 435),
        ]);
        $discountCollection = new DiscountCollection();
        $discount = new CountDiscount(4, 10);
        $discount->setIgnoreProductPrimaryKeys(['A']);
        $discountCollection->setDiscount($discount);

        $store = new DiscountCalculator($productCollection, $discountCollection);
        $this->assertEquals(1855, $store->getTotalPriceWithDiscount());
    }

    /**
     * Проверяем, что ни один товар не пересекается, дефолт
     * 550 + 390 = 940
     * @throws InvalidTypeException
     */
    public function testPairDiscountNoEntry(): void
    {
        $productCollection = new ProductCollection([
            new Product('A', 550),
            new Product('B', 390),
        ]);
        $discountCollection = new DiscountCollection([
            new PairDiscount(['C', 'D'], 10)
        ]);

        $store = new DiscountCalculator($productCollection, $discountCollection);
        $this->assertEquals(940, $store->getTotalPriceWithDiscount());
    }
}

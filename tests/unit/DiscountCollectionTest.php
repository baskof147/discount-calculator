<?php

declare(strict_types=1);

namespace unit;

use baskof147\discount_calculator\discount\DiscountCollection;
use baskof147\discount_calculator\exceptions\InvalidTypeException;
use Codeception\Test\Unit;

/**
 * Class DiscountCollectionTest
 * @package unit
 */
class DiscountCollectionTest extends Unit
{
    public function testInvalidTypeException(): void
    {
        $this->expectException(InvalidTypeException::class);
        new DiscountCollection([1, 2, 3]);
    }
}
